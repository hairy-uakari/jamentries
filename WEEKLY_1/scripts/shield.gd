extends Area2D

onready var player_node = $".."/".."

signal bullet_increase

func _ready():
	connect("body_entered", self, "handle_bullet")

func _process(delta):
	pass

func handle_bullet(body):
	if body.name == "enemy_bullet":
		print("AREA")
		body.queue_free()
		if body.index == player_node.current_sprite:
			player_node.bullet_count += 1
			emit_signal("bullet_increase")
