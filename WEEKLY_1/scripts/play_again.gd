extends Button


func _ready():
	connect("pressed", self, "on_press_restart")

func on_press_restart():
	get_tree().change_scene("res://scenes/main.tscn")
