extends Timer

onready var level = $".."

var locs = [-10, 1050]

func _ready():
	connect("timeout", self, "spawn_enemy")

func spawn_enemy():
	print("Enemy Spawned")
	var enemy_inst = load("res://scenes/enemy.tscn")
	var enemy = enemy_inst.instance()
	wait_time = rand_range(5, 8)
	enemy.position = get_spawn_location()
	level.add_child(enemy)

func get_spawn_location() -> Vector2:
	if randi() % 2 == 0: # horizontal spawn
		var y = randi() % 2
		var x = randi() % 1024
		print("POS : ", x)
		return Vector2(x, locs[y])
	else: # vertical spawn
		var x = randi() % 2
		var y = randi() % 600
		return Vector2(locs[x], y)
