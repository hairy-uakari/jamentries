extends KinematicBody2D

onready var power_node = $power
onready var power_state = randi() % 3
onready var powers = [load("res://textures/Paper.png"),\
					  load("res://textures/Scissors.png"),\
					  load("res://textures/Rock.png")]
var velocity: Vector2 = Vector2.ZERO
onready var player_node = $"../player"

var is_player_dead = false

func _ready():
	player_node.connect("player_dead", self, "handle_player_death")
	power_node.texture = powers[power_state]

func _physics_process(delta):
	if !is_player_dead:
		velocity = steering.steer(position, player_node.position,\
				   velocity)
	move_and_slide(velocity)

func _process(delta):
	if !is_player_dead:
		steering.handle_lookat(self, player_node.position)

func shoot():
	var level_node = $".."
	var bullet_inst = load("res://scenes/enemy_bullet.tscn")
	var bullet = bullet_inst.instance()
	bullet.spawn(power_node.texture, global_position, global_rotation, power_state)
	level_node.add_child(bullet)

func handle_player_death():
	is_player_dead = true
