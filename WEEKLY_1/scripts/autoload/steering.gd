	extends Node

const DEFAULT_MASS: float = 2.0
const DEFAULT_MAX_SPEED: float = 400.0

static func steer(
		from: Vector2,
		to: Vector2,
		velocity: Vector2,
		mass: float = DEFAULT_MASS,
		max_speed: float = DEFAULT_MAX_SPEED
	) -> Vector2:
	if from.distance_to(to) <= 500:
		if from.distance_to(to) <= 200:
			return Vector2.ZERO
		var val = from.distance_to(to) - 200
		max_speed = (val / 300) * max_speed
	var resultant_velocity = (to - from).normalized() * max_speed
	var steering_velocity = (resultant_velocity - velocity) / mass

	return velocity + steering_velocity

static func handle_lookat(enemy_node: Node2D, player_pos: Vector2):
	enemy_node.look_at((player_pos))
