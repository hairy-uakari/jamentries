extends KinematicBody2D

var motion: Vector2 = Vector2.ZERO
var direction: Vector2 = Vector2.ZERO
export var speed: float = 10
var index = -1
onready var bullet_node = $".."/bullets

func _ready():
	motion += direction * speed

func _physics_process(delta):
	var collision_info = move_and_collide(motion)
	if collision_info:
		queue_free()
		if collision_info.collider.name == "shield":
			var player = collision_info.collider.get_owner()
			if index == player.current_sprite:
				player.update_bullets(1)
			else:
				player.reduce_health()
		else:
			var player = collision_info.collider
			player.reduce_health()


func spawn(texture, _position, _rotation, _index):
	$Sprite.texture = texture
	rotation = _rotation
	index = _index
	var angle = _rotation
	direction = Vector2(cos(angle), sin(angle)).normalized()
	position = _position + direction * 30
