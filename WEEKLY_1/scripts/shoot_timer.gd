extends Timer

onready var enemy_node = $".."

func _ready():
	wait_time = rand_range(1, 3)
	connect("timeout", self, "shoot")

func shoot():
	wait_time = rand_range(1, 3)
	enemy_node.shoot()
