extends KinematicBody2D

signal player_dead
signal bullet_updated(updated_val)

var motion: Vector2 = Vector2.ZERO
var MAX_SPEED: float = 600
var ACC: float = 2000

var current_health: float = 1.0

onready var power_sprite_node = $Power
onready var power_sprites = [load("res://textures/Paper.png"),\
							 load("res://textures/Scissors.png"),\
							 load("res://textures/Rock.png")]

var current_sprite = 0
var current_power_changed = false
var bullet_count = 5

var prev_look_at: Vector2 = Vector2(-10000, -10000)

func _ready():
	connect("bullet_updated", $".."/bullets, "on_update_bullets")
	power_sprite_node.texture = power_sprites[current_sprite]

func _process(dt):
	player_should_die()
	handle_mouse()
	handle_power_change()

func _physics_process(dt):
	if Input.is_action_just_released("shoot") and bullet_count > 0:
		shoot()
	var axis = get_axis()
	if axis == Vector2.ZERO:
		apply_friction(dt * ACC)
	else:
		apply_movement(dt * ACC * axis)
	
	motion = move_and_slide(motion)

func get_axis() -> Vector2:
	var axis: Vector2 = Vector2.ZERO
	axis.x = int(Input.is_action_pressed("move_right")) - \
			 int(Input.is_action_pressed("move_left"))
	axis.y = int(Input.is_action_pressed("move_down")) - \
			 int(Input.is_action_pressed("move_up"))
	return axis.normalized()

func apply_movement(amount: Vector2):
	motion += amount
	if motion.length() > MAX_SPEED:
		motion = motion.normalized() * MAX_SPEED

func apply_friction(amount: float):
	if motion.length() < amount:
		motion = Vector2.ZERO
	else:
		motion -= motion.normalized() * amount

func handle_mouse():
	if prev_look_at == Vector2(-10000, -10000):
		prev_look_at = get_global_mouse_position()
		look_at(prev_look_at)
	else:
		var mouse_pos = get_global_mouse_position()
		var current_pos = lerp(prev_look_at, mouse_pos, 0.5)
		look_at(current_pos)
		prev_look_at = current_pos

func handle_power_change(scroll = -1):
	if Input.is_action_just_released("cycle_left") or scroll == 0:
		current_sprite -= 1
		if current_sprite < 0:
			current_sprite = 2
		current_power_changed = true
	if Input.is_action_just_released("cycle_right") or scroll == 1:
		current_sprite += 1
		current_sprite = current_sprite % 3
		current_power_changed = true
	
	if current_power_changed:
		power_sprite_node.texture = power_sprites[current_sprite]
		current_power_changed = false

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_UP:
			handle_power_change(0)
		if event.button_index == BUTTON_WHEEL_DOWN:
			handle_power_change(1)

func shoot():
	update_bullets(-1)
	var level_node = $".."
	var bullet_inst = load("res://scenes/bullet.tscn")
	var bullet = bullet_inst.instance()
	level_node.add_child(bullet)

func reduce_health():
	current_health -= 0.1
	var health_bar = $bar
	health_bar.material.set_shader_param("health", current_health)
	
func player_should_die():
	if current_health <= 0:
		emit_signal("player_dead")
		var score = $".."/score.score
		Globals.score = score
		queue_free()
		get_tree().change_scene("res://scenes/end.tscn")

func update_bullets(amt):
	bullet_count += amt
	emit_signal("bullet_updated", bullet_count)

