extends KinematicBody2D

signal enemy_shot

var motion: Vector2 = Vector2.ZERO
var direction: Vector2 = Vector2.ZERO
export var speed: float = 15

func _ready():
	var parent_node: Node2D = $".."/player
	rotation = parent_node.rotation
	var angle: float = parent_node.rotation
	direction = Vector2(cos(angle), sin(angle)).normalized()
	position = parent_node.global_position + direction * 30
	motion += direction * speed
	
func _physics_process(delta):
	var col_info = move_and_collide(motion)
	if col_info:
		var enemy = col_info.collider
		queue_free()
		enemy.queue_free()
		var score_node: RichTextLabel = $".."/score
		score_node.score += 1
		score_node.text = "SCORE: " + str(score_node.score)
