extends Button

func _ready():
	connect("pressed", self, "on_button_press")

func on_button_press():
	get_tree().change_scene("res://scenes/main.tscn")
