shader_type canvas_item;

void fragment() {
	vec2 uv = UV;
	vec2 waves;
	waves.x = cos(TIME + (uv.x + uv.y));
	waves.y = sin(TIME + (uv.x + uv.y));
	COLOR = texture(TEXTURE, uv + waves * 0.07);
}