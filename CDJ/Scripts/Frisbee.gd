extends Area2D

onready var enemy_node: Node2D = get_node('/root/Level/Enemy')
onready var player_node: Node2D = get_node("/root/Level/KinematicBody2D")
var docked: bool = true

onready var host = player_node

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "handle_enter")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if docked:
		position = host.position
		position += Vector2(10, -30)

func handle_enter(body):
	if body.get_name() == "Enemy" or body.get_name() == "KinematicBody2D":
		docked = true
		host = body
		position = host.position
		position += Vector2(10, -30)

func is_player_host():
	return host.get_name() == "KinematicBody2D"

func is_enemy_host():
	return host.get_name() == "Enemy"
