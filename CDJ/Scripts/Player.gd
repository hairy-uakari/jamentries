extends KinematicBody2D

var MAX_SPEED: float = 600
var ACC: float = 2000
var motion: Vector2 = Vector2(0, 0)

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	var axis: Vector2 = get_axis()
	if axis == Vector2.ZERO:
		apply_friction(ACC * delta)
	else:
		apply_movement(axis * ACC * delta)
	
	motion = move_and_slide(motion)
	position.y = clamp(position.y, 300, 1000)

func get_axis() -> Vector2:
	var axis: Vector2 = Vector2.ZERO
	axis.x = int(Input.is_action_pressed("move_right")) - \
	int(Input.is_action_pressed("move_left"))
	axis.y = int(Input.is_action_pressed("move_down")) - \
	int(Input.is_action_pressed("move_up"))
	return axis.normalized()

func apply_friction(amount: float) -> void:
	if motion.length() > amount:
		motion -= motion.normalized() * amount
	else:
		motion = Vector2.ZERO

func apply_movement(amount: Vector2):
	motion += amount
	if motion.length() >= MAX_SPEED:
		motion = motion.normalized() * MAX_SPEED






