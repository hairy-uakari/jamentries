extends KinematicBody2D

var motion = Vector2.ZERO
var MAX_SPEED = 500
var ACC = 100

onready var level = $".."

enum States {
	IDLE,
	MOVING
}

var current_state = States.IDLE
var target_pos = null

var timer = 0

onready var frisbee = $"../Area2D"

func _ready():
	pass
	
func _physics_process(delta):
	if current_state != States.MOVING:
		target_pos = should_get_position(delta)
	if target_pos != null and current_state == States.MOVING:
		var direction = get_direction(target_pos)
		if direction != Vector2.ZERO:
			apply_movement(direction * ACC * delta)
		else:
			current_state = States.IDLE
			motion = Vector2.ZERO
	var collision_info = move_and_collide(motion)
	if collision_info:
		current_state = States.IDLE
		motion = Vector2.ZERO

func get_position() -> Vector2:
	return level.end_point

func get_direction(to: Vector2) -> Vector2:
	var vec = to - position
	if abs(vec.x) <= 2 and abs(vec.y) <= 2:
		return Vector2.ZERO
	return vec.normalized()

func apply_movement(amount: Vector2):
	motion += amount
	if motion.length() > (target_pos - position).length():
		motion = motion.normalized() * (target_pos - position).length()
	elif motion.length() >= MAX_SPEED:
		motion = motion.normalized() * MAX_SPEED

func should_get_position(dt):
	if current_state == States.IDLE and !level.frisbee.docked and \
		frisbee.is_player_host():
			current_state = States.MOVING
			timer = 0
			return get_position()
	timer += dt
	return null

