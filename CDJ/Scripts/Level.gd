extends Node2D

enum MouseState {
	RELEASED,
	CLICKED
}

var mouse_state = MouseState.RELEASED

var should_draw = false

var launcher = null

onready var frisbee: Node2D = $Area2D

var end_point
var start_point
var control_point

func _ready():
	pass

func _process(delta):
	has_game_ended()
	if !frisbee.docked:
		if launcher is GDScriptFunctionState and launcher.is_valid():
			launcher = launcher.resume(delta)
	if valid_shot():
		mouse_state = MouseState.CLICKED
		end_point = get_global_mouse_position()
		should_draw = true
	if valid_release():
		mouse_state = MouseState.RELEASED
		should_draw = false
		start_point = frisbee.position
		control_point = get_global_mouse_position()
		launcher = launch_frisbee(delta)
	if frisbee.is_enemy_host() and frisbee.docked:
		shoot_prepare_enemy()
		launcher = launch_frisbee(delta)

func _input(event):
	if event is InputEventMouseMotion and \
		mouse_state ==  MouseState.CLICKED:
			pass

func valid_shot() -> bool:
	return Input.is_mouse_button_pressed(BUTTON_LEFT) and \
		frisbee.docked and mouse_state == MouseState.RELEASED \
		and frisbee.is_player_host()

func valid_release() -> bool:
	return !Input.is_mouse_button_pressed(BUTTON_LEFT) and \
		frisbee.docked and mouse_state == MouseState.CLICKED and \
		frisbee.is_player_host()

func launch_frisbee(dt):
	frisbee.docked = false
	var t: float = 0
	while t <= 1:
		var start_handle = (1 - t) * (1 - t)
		var control_handle = 2 * (1 - t) * t
		var end_handle = t * t
		var point = start_point * start_handle + \
					control_point * control_handle + \
					end_point * end_handle
		frisbee.set_position(point)
		t += dt
		if t > 1:
			t = 1
		yield()

func shoot_prepare_enemy():
	start_point = frisbee.position
	var x = rand_range(122, 921)
	var y = rand_range(300, 448)
	end_point = Vector2(x, y)
	control_point = Vector2(rand_range(100, 500), rand_range(200, 700))

func has_game_ended():
	if end_point != null:
		print(frisbee.position, ' ', end_point)
	if !frisbee.docked and (frisbee.position - end_point).length() <= 0.0001:
		print('Ended')

func vec_abs(vec: Vector2):
	return Vector2(abs(vec.x), abs(vec.y))



