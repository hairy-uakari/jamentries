extends Line2D

onready var level_node = $".."
onready var frisbee = $"../Area2D"

func _ready():
	pass

func _process(delta):
	if points.size() > 0:
		clear_points()
	if level_node.should_draw:
		draw_bezier()

func draw_bezier():
	for i in range(0,100):
		var t = i / 100.0
		var start_handle = (1 - t) * (1 - t)
		var control_handle = 2 * (1 - t) * t
		var end_handle = t * t
		var point = frisbee.position * start_handle + \
					get_global_mouse_position() * control_handle + \
					level_node.end_point * end_handle
		add_point(point)
